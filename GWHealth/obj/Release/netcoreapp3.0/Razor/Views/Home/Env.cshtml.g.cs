#pragma checksum "C:\Users\kipi\source\repos\GWHealth\GWHealth\Views\Home\Env.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "5d001cc87a7a2741d52231380e205847be66e4cd"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_Env), @"mvc.1.0.view", @"/Views/Home/Env.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\kipi\source\repos\GWHealth\GWHealth\Views\_ViewImports.cshtml"
using GWHealth;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\kipi\source\repos\GWHealth\GWHealth\Views\_ViewImports.cshtml"
using GWHealth.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"5d001cc87a7a2741d52231380e205847be66e4cd", @"/Views/Home/Env.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"07513305e4770ab9f4e27296f014037d3f333eb0", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_Env : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<LoginViewModel>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-controller", "Home", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Index", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Main", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/Server.png"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n");
#nullable restore
#line 3 "C:\Users\kipi\source\repos\GWHealth\GWHealth\Views\Home\Env.cshtml"
  
    ViewData["Title"] = "Env";

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n<nav aria-label=\"breadcrumb\">\r\n    <ol class=\"breadcrumb\">\r\n        <li class=\"breadcrumb-item\">");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "5d001cc87a7a2741d52231380e205847be66e4cd4761", async() => {
                WriteLiteral("Home");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Controller = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("</li>\r\n        <li class=\"breadcrumb-item\">");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "5d001cc87a7a2741d52231380e205847be66e4cd6163", async() => {
                WriteLiteral("Main");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Controller = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_2.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_2);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("</li>\r\n        <li class=\"breadcrumb-item active\" aria-current=\"page\">");
#nullable restore
#line 11 "C:\Users\kipi\source\repos\GWHealth\GWHealth\Views\Home\Env.cshtml"
                                                          Write(Model.ActiveEnvirnment.Name);

#line default
#line hidden
#nullable disable
            WriteLiteral(@"</li>

    </ol>
</nav>

<div class=""container"">
    <div class=""row"">
        <div class=""btn-toolbar"" role=""toolbar"" aria-label=""Toolbar with button groups"">

            <div class=""btn-group mr-2"" role=""group"" aria-label=""Second group"">
                <button type=""button"" class=""btn btn-secondary"">AdminLoad</button>
            </div>
            <div class=""btn-group"" role=""group"" aria-label=""Third group"">
                <button type=""button"" class=""btn btn-secondary"">Health</button>
            </div>
            <div class=""btn-group"" role=""group"" aria-label=""Third group"">
                <button type=""button"" class=""btn btn-secondary"">Release Histry</button>
            </div>
        </div>

    </div>

</div>

<div class=""container"">
        <div class=""row""></div>
");
#nullable restore
#line 37 "C:\Users\kipi\source\repos\GWHealth\GWHealth\Views\Home\Env.cshtml"
         if (@Model.ActiveEnvirnment.GWMachines != null )
        {


            

#line default
#line hidden
#nullable disable
#nullable restore
#line 41 "C:\Users\kipi\source\repos\GWHealth\GWHealth\Views\Home\Env.cshtml"
             if (Model.ActiveEnvirnment.IsClusterd)
            {

#line default
#line hidden
#nullable disable
            WriteLiteral("                <div class=\"row\">\r\n");
#nullable restore
#line 44 "C:\Users\kipi\source\repos\GWHealth\GWHealth\Views\Home\Env.cshtml"
                     foreach (var machine in @Model.ActiveEnvirnment.GWMachines)
                    {
                        if (@machine.isBatch)
                        {

#line default
#line hidden
#nullable disable
            WriteLiteral("                            <div class=\"col\">\r\n                                <div class=\"card text-center\" style=\"width: 12rem;\">\r\n                                    <div class=\"card-body\">\r\n                                        ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "5d001cc87a7a2741d52231380e205847be66e4cd9856", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n                                        <h5 class=\"card-title\">");
#nullable restore
#line 52 "C:\Users\kipi\source\repos\GWHealth\GWHealth\Views\Home\Env.cshtml"
                                                          Write(machine.Name);

#line default
#line hidden
#nullable disable
            WriteLiteral("</h5>\r\n                                        <p class=\"card-text\">");
#nullable restore
#line 53 "C:\Users\kipi\source\repos\GWHealth\GWHealth\Views\Home\Env.cshtml"
                                                        Write(machine.Description);

#line default
#line hidden
#nullable disable
            WriteLiteral("</p>\r\n                                        <p class=\"card-text\">");
#nullable restore
#line 54 "C:\Users\kipi\source\repos\GWHealth\GWHealth\Views\Home\Env.cshtml"
                                                        Write(machine.IPAddress);

#line default
#line hidden
#nullable disable
            WriteLiteral("</p>\r\n                                        <p class=\"card-text\">");
#nullable restore
#line 55 "C:\Users\kipi\source\repos\GWHealth\GWHealth\Views\Home\Env.cshtml"
                                                        Write(machine.ExternalName);

#line default
#line hidden
#nullable disable
            WriteLiteral(@"</p>
                                        <a href=""#"" class=""btn btn-primary"">B</a>
                                        <a href=""#"" class=""btn btn-primary"">D</a>
                                        <a href=""#"" class=""btn btn-primary"">R</a>
                                        <a href=""#"" class=""btn btn-primary"">L</a>
                                        <a href=""#"" class=""btn btn-primary"">S</a>
                                    </div>
                                </div>
                            </div>
");
#nullable restore
#line 64 "C:\Users\kipi\source\repos\GWHealth\GWHealth\Views\Home\Env.cshtml"
                        }
                    }

#line default
#line hidden
#nullable disable
            WriteLiteral("                </div>\r\n");
            WriteLiteral("                <div class=\"row\">\r\n");
#nullable restore
#line 69 "C:\Users\kipi\source\repos\GWHealth\GWHealth\Views\Home\Env.cshtml"
                     foreach (var machine in @Model.ActiveEnvirnment.GWMachines)
                    {
                        if (!@machine.isBatch)
                        {

#line default
#line hidden
#nullable disable
            WriteLiteral("                            <div class=\"col\">\r\n                                <div class=\"card text-center\" style=\"width: 12rem;\">\r\n                                    <div class=\"card-body\">\r\n                                        ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "5d001cc87a7a2741d52231380e205847be66e4cd13709", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n                                        <h5 class=\"card-title\">");
#nullable restore
#line 77 "C:\Users\kipi\source\repos\GWHealth\GWHealth\Views\Home\Env.cshtml"
                                                          Write(machine.Name);

#line default
#line hidden
#nullable disable
            WriteLiteral("</h5>\r\n                                        <p class=\"card-text\">");
#nullable restore
#line 78 "C:\Users\kipi\source\repos\GWHealth\GWHealth\Views\Home\Env.cshtml"
                                                        Write(machine.Description);

#line default
#line hidden
#nullable disable
            WriteLiteral("</p>\r\n                                        <p class=\"card-text\">");
#nullable restore
#line 79 "C:\Users\kipi\source\repos\GWHealth\GWHealth\Views\Home\Env.cshtml"
                                                        Write(machine.IPAddress);

#line default
#line hidden
#nullable disable
            WriteLiteral("</p>\r\n                                        <p class=\"card-text\">");
#nullable restore
#line 80 "C:\Users\kipi\source\repos\GWHealth\GWHealth\Views\Home\Env.cshtml"
                                                        Write(machine.ExternalName);

#line default
#line hidden
#nullable disable
            WriteLiteral(@"</p>
                                        <a href=""#"" class=""btn btn-primary"">B</a>
                                        <a href=""#"" class=""btn btn-primary"">D</a>
                                        <a href=""#"" class=""btn btn-primary"">R</a>
                                        <a href=""#"" class=""btn btn-primary"">L</a>
                                        <a href=""#"" class=""btn btn-primary"">S</a>
                                    </div>
                                </div>
                            </div>
");
#nullable restore
#line 89 "C:\Users\kipi\source\repos\GWHealth\GWHealth\Views\Home\Env.cshtml"
                        }
                    }

#line default
#line hidden
#nullable disable
            WriteLiteral("                </div>\r\n");
#nullable restore
#line 92 "C:\Users\kipi\source\repos\GWHealth\GWHealth\Views\Home\Env.cshtml"

            }
            else
            {

#line default
#line hidden
#nullable disable
            WriteLiteral("                <div class=\"row\">\r\n");
#nullable restore
#line 97 "C:\Users\kipi\source\repos\GWHealth\GWHealth\Views\Home\Env.cshtml"
                     foreach (var machine in @Model.ActiveEnvirnment.GWMachines)
                    {

#line default
#line hidden
#nullable disable
            WriteLiteral("                        <div class=\"col\">\r\n                            <div class=\"card text-center\" style=\"width: 12rem;\">\r\n                                <div class=\"card-body\">\r\n                                    ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "5d001cc87a7a2741d52231380e205847be66e4cd17670", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n                                    <h5 class=\"card-title\">");
#nullable restore
#line 103 "C:\Users\kipi\source\repos\GWHealth\GWHealth\Views\Home\Env.cshtml"
                                                      Write(machine.Name);

#line default
#line hidden
#nullable disable
            WriteLiteral("</h5>\r\n                                    <p class=\"card-text\">");
#nullable restore
#line 104 "C:\Users\kipi\source\repos\GWHealth\GWHealth\Views\Home\Env.cshtml"
                                                    Write(machine.Description);

#line default
#line hidden
#nullable disable
            WriteLiteral("</p>\r\n                                    <p class=\"card-text\">");
#nullable restore
#line 105 "C:\Users\kipi\source\repos\GWHealth\GWHealth\Views\Home\Env.cshtml"
                                                    Write(machine.IPAddress);

#line default
#line hidden
#nullable disable
            WriteLiteral("</p>\r\n                                    <p class=\"card-text\">");
#nullable restore
#line 106 "C:\Users\kipi\source\repos\GWHealth\GWHealth\Views\Home\Env.cshtml"
                                                    Write(machine.ExternalName);

#line default
#line hidden
#nullable disable
            WriteLiteral(@"</p>
                                    <a href=""#"" class=""btn btn-primary"">B</a>
                                    <a href=""#"" class=""btn btn-primary"">D</a>
                                    <a href=""#"" class=""btn btn-primary"">R</a>
                                    <a href=""#"" class=""btn btn-primary"">L</a>
                                    <a href=""#"" class=""btn btn-primary"">S</a>
                                </div>
                            </div>
                        </div>
");
#nullable restore
#line 115 "C:\Users\kipi\source\repos\GWHealth\GWHealth\Views\Home\Env.cshtml"

                    }

#line default
#line hidden
#nullable disable
            WriteLiteral("                </div>\r\n");
#nullable restore
#line 118 "C:\Users\kipi\source\repos\GWHealth\GWHealth\Views\Home\Env.cshtml"

            }

#line default
#line hidden
#nullable disable
#nullable restore
#line 119 "C:\Users\kipi\source\repos\GWHealth\GWHealth\Views\Home\Env.cshtml"
             



        }
        else
        {

#line default
#line hidden
#nullable disable
            WriteLiteral("            <div class=\"alert alert-danger\" role=\"alert\">\r\n                There are no machines\r\n            </div>\r\n");
#nullable restore
#line 129 "C:\Users\kipi\source\repos\GWHealth\GWHealth\Views\Home\Env.cshtml"
        }

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n    </div>\r\n\r\n\r\n");
#nullable restore
#line 134 "C:\Users\kipi\source\repos\GWHealth\GWHealth\Views\Home\Env.cshtml"
     if (Model.ActiveEnvirnment.IsClusterd)
    {

#line default
#line hidden
#nullable disable
            WriteLiteral("        <div class=\"alert alert-primary\" role=\"alert\">\r\n            This is clusterd Env\r\n        </div>\r\n");
#nullable restore
#line 139 "C:\Users\kipi\source\repos\GWHealth\GWHealth\Views\Home\Env.cshtml"

    }

#line default
#line hidden
#nullable disable
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<LoginViewModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
