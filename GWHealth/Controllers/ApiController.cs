﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using GWHealth.Filters;
namespace GWHealth.Controllers
{

    [Route("API")]
    [SecureRequest]
    public class ApiController : Controller
    {
        [HttpGet("About")]

        public ContentResult About(string envname , string userId)
        {
                         

            return new ContentResult
            {
                ContentType = "text/html",
                Content = "<div>Requested by "+ userId  + "</div>"
            };
        }



    }
}